package Ejercicio1;

import javax.swing.JOptionPane;

public class MayorMenorApp {
	/* Autor: Alejandro Largo Castrillon
	 	Declara 2 variables num�ricas (con el valor que desees), he indica cual es mayor de los
		dos. Si son iguales indicarlo tambi�n. Ves cambiando los valores para comprobar que funciona.
	 */

	public static void main(String[] args) {
		int valor1 = 80, valor2 = 60;
		
		//se hace una condicion para comprobar cual es el numero mayor o su igualdad 
		if(valor1>valor2) {
			JOptionPane.showMessageDialog(null, valor1 + " es mayor ");
		}else if(valor2>valor1) {
			JOptionPane.showMessageDialog(null, valor2 + " es mayor ");
		}else {
			JOptionPane.showMessageDialog(null, valor2 + " es mayor ");
		}
		

	}

}
