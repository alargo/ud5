package Ejercicio10;

import javax.swing.JOptionPane;

public class Ejercicio10App {

	/* Autor: Alejandro Largo Castrillon
	 * 
 	  Realiza una aplicaci�n que nos pida un n�mero de ventas a introducir, despu�s nos
pedir� tantas ventas por teclado como n�mero de ventas se hayan indicado. Al final
mostrara la suma de todas las ventas. Piensa que es lo que se repite y lo que no.

 */
	public static void main(String[] args) {
		String NumVentas = JOptionPane.showInputDialog("Introduce numero de ventas");

        // Se Pasa a numero las ventas
        int ventas = Integer.parseInt(NumVentas);

       
        double Total = 0;

        //Bucle para ir sumando los valores de los productos
        for(int contador=1; contador<=ventas;contador++) {
        	NumVentas = JOptionPane.showInputDialog("Introduce valor del producto "+contador);
        double valor = Double.parseDouble(NumVentas);
        Total = Total + valor;
 
        }

        JOptionPane.showMessageDialog(null, "La suma total de todas las ventas es: " + Total);

	}

}
