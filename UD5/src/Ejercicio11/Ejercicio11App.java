package Ejercicio11;

import javax.swing.JOptionPane;

public class Ejercicio11App {
	/* Autor: Alejandro Largo Castrillon
	 * 
 	 Crea una aplicaci�n que nos pida un d�a de la semana y que nos diga si es un d�a laboral
o no. Usa un switch para ello.

 */

	public static void main(String[] args) {
		String dia = JOptionPane.showInputDialog("Introduce un dia de la semana");
		
		switch (dia) {
		case "Lunes":
			JOptionPane.showMessageDialog(null,"Dia Laboral");
			break;
		case "Martes":
			JOptionPane.showMessageDialog(null,"Dia Laboral");
			break;
		case "Miercoles":
			JOptionPane.showMessageDialog(null,"Dia Laboral");
			break;
		case "Jueves":
			JOptionPane.showMessageDialog(null,"Dia Laboral");
			break;
		case "Viernes":
			JOptionPane.showMessageDialog(null,"Dia Laboral");
			break;
		case "Sabado":
			JOptionPane.showMessageDialog(null,"Dia Laboral");
			break;
		case "Domingo":
			JOptionPane.showMessageDialog(null,"Dia no Laboral");
			break;
			
		default:
			JOptionPane.showMessageDialog(null,"No has introducido un dia de la semana");
		}

	}

}
