package Ejercicio12;

import javax.swing.JOptionPane;

public class Ejercicio12App {

	public static void main(String[] args) {
		/* Autor: Alejandro Largo Castrillon
		 * 
	 	 Escribe una aplicaci�n con un String que contenga una contrase�a cualquiera. Despu�s
se te pedir� que introduzcas la contrase�a, con 3 intentos. Cuando aciertes ya no pedir� mas
la contrase�a y mostrara un mensaje diciendo �Enhorabuena�. Piensa bien en la condici�n
de salida (3 intentos y si acierta sale, aunque le queden intentos).

	 */
		
		int intentos = 0;
		//se crea un booleano que empezara en falso porque no se ha acertado todavia la contrase�a
        boolean acierto = false;
        String contrase�a = JOptionPane.showInputDialog("Escribe una contrase�a");
        do {
            String entrada = JOptionPane.showInputDialog("Introduce contrase�a");

            //Una condicion para comprobar que la contras�ea que  se introdujo dea la misma que se pone despues 
            if(contrase�a.equals(entrada)) {
                JOptionPane.showMessageDialog(null, "Cohinciden");
                acierto=true;
            }else {
                intentos++;
            }


        }
        while(intentos<=3 && !acierto);

	}

}
