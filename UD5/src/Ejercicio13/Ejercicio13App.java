package Ejercicio13;

import javax.swing.JOptionPane;

public class Ejercicio13App {
	
	/* Autor: Alejandro Largo Castrillon
	 * 
 	 Crea una aplicaci�n llamada CalculadoraInversa, nos pedir� 2 operandos (int) y un signo aritm�tico (String), 
 	 seg�n este �ltimo se realizara la operaci�n correspondiente. Al final mostrara el resultado en un cuadro de dialogo.
	Los signos aritm�ticos disponibles son:
+: suma los dos operandos.
-: resta los operandos.
*: multiplica los operandos.
/: divide los operandos, este debe dar un resultado con decimales (double)
^: 1� operando como base y 2� como exponente.
%: m�dulo, resto de la divisi�n entre operando1 y operando2.

 */

	public static void main(String[] args) {
		String num1 = JOptionPane.showInputDialog("Introduce un numero");
		int numN1 = Integer.parseInt(num1);
		
		String num2 = JOptionPane.showInputDialog("Introduce un numero");
		int numN2 = Integer.parseInt(num2);
		
		String signo = JOptionPane.showInputDialog("Introduce un signo aritmetico");
		
		//se le pasa los numeros y depende del signo hara una operacion diferente 
		switch (signo) {
		case "+":
			int suma = numN1 + numN2;
			JOptionPane.showMessageDialog(null,suma);
			break;
		case "-":
			int resta = numN1 - numN2;
			JOptionPane.showMessageDialog(null,resta);
			break;
		case "*":
			int mult = numN1 * numN2;
			JOptionPane.showMessageDialog(null,mult);
			break;
		case "/":
			int div = numN1 / numN2;
			JOptionPane.showMessageDialog(null,div);
			break;
		case "^":
			double expo = Math.pow(numN1, numN2);
			JOptionPane.showMessageDialog(null,expo);
			break;
		case "%":
			int residuo = numN1 % numN2;
			JOptionPane.showMessageDialog(null,residuo);
			break;
			
		default:
			JOptionPane.showMessageDialog(null,"No has introducido un signo valido");
		}

	}

}
