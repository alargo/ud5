package Ejercicio2;

import javax.swing.JOptionPane;

public class Ejercicio2App {
	/* Autor: Alejandro Largo Castrillon
	 * 
 	Declara un String que contenga tu nombre, despu�s muestra un mensaje de bienvenida por consola.
 	 Por ejemplo: si introduzco �Fernando�, me aparezca �Bienvenido Fernando�.
 */
	public static void main(String[] args) {
		String nombre = "alejandro";
		
		JOptionPane.showMessageDialog(null,"Bienvenido " + nombre);

	}

}
