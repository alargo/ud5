package Ejercicio3;

import javax.swing.JOptionPane;

public class Ejercicio3App {

	/* Autor: Alejandro Largo Castrillon
	 * 
 	Modifica la aplicación anterior, para que nos pida el nombre que queremos introducir
(recuerda usar JOptionPane).
 */
	public static void main(String[] args) {
		String nombre = JOptionPane.showInputDialog("Introduce tu nombre");
		//se concatena la palabra Bienvenido con el nombre que se le pasa
		JOptionPane.showMessageDialog(null,"Bienvenido " + nombre);

	}

}
