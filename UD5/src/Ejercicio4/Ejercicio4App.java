package Ejercicio4;

import javax.swing.JOptionPane;

public class Ejercicio4App {
	
	/* Autor: Alejandro Largo Castrillon
	 * 
 	Haz una aplicaci�n que calcule el �rea de un circulo (pi*R2). El radio se pedir� por teclado
(recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el
m�todo pow de Math.
 */

	public static void main(String[] args) {
		
		// Se introduce el valor y se pasa de String a numero y luego con el Math.pow se hace la operacion matematica
		String radio= JOptionPane.showInputDialog("Introduce el radio");
		
		double radioN = Double.parseDouble(radio);
		
		double result = Math.PI * Math.pow(radioN, 2);
		
		JOptionPane.showMessageDialog(null,"El area de un circulo es " + result);
		
		
		

	}

}
