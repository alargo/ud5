package Ejercicio5;

import javax.swing.JOptionPane;

public class Ejercicio5App {
	/* Autor: Alejandro Largo Castrillon
	 * 
 	Lee un n�mero por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es, tambi�n
debemos indicarlo.
 */

	public static void main(String[] args) {
		String numero = JOptionPane.showInputDialog("Introduce tu numero");
		
		int numeroN=Integer.parseInt(numero);
		
		if(numeroN % 2 == 0) {
			JOptionPane.showMessageDialog(null,"Es divisible entre 2 ");
		}else {
			JOptionPane.showMessageDialog(null,"No es divisible entre 2 ");
		}

	}

}
