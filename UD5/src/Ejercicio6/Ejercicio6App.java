package Ejercicio6;

import javax.swing.JOptionPane;

public class Ejercicio6App {

	public static void main(String[] args) {
		/* Autor: Alejandro Largo Castrillon
		 * 
	 	 Lee un n�mero por teclado que pida el precio de un producto (puede tener decimales) y
calcule el precio final con IVA. El IVA sera una constante que sera del 21%

	 */
		
		String precio= JOptionPane.showInputDialog("Introduce el precio");
		
		double precioN = Double.parseDouble(precio);
		
		double precioFinal = precioN + (precioN * 0.21);
		
		JOptionPane.showMessageDialog(null,"El precio fina es " + precioFinal);

	}

}
