package Ejercicio7;

public class Ejercicio7App {

	public static void main(String[] args) {
		/* Autor: Alejandro Largo Castrillon
		 * 
	 	 Muestra los n�meros del 1 al 100 (ambos incluidos). Usa un bucle while.

	 */
		
		int num=1;
		
		while(num<=100) {
			System.out.println(num);
			num++;
		}
	}

}
